import React, {Component} from 'react';
import {hot} from 'react-hot-loader';
import ReactPlayer from './components/ReactPlayer';

export class App extends Component {
    ref = player => {
        this.player = player
    };

    render() {
        return (
            <div className="App" style={{padding: 30, textAlign: 'center'}}>
                <ReactPlayer ref={this.ref}/>
            </div>
        );
    }
}

export default hot(module)(App);
