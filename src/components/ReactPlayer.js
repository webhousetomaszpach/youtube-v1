import React, {Component} from 'react';
import YTPlayer from "./YTPlayer";

export class ReactPlayer extends Component {
    ref = container => {
        this.container = container
    };

    render() {
        return (
            <div className='react-player' ref={this.ref}>
                <YTPlayer/>
            </div>
        );
    }
}

export default ReactPlayer;